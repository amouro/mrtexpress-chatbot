const fs = require('fs');
const gmap = require('@google/maps');
const api_key = "AIzaSyD4u35i9J1RUVWawLlsgprzl6hc-jvVQ2g";
var googleMapsClient = gmap.createClient({
  key: api_key
});

let exits;
fs.readFile('data/stationExits.json', 'utf8', function (err, data) {
  if (err) throw err;
  exits = JSON.parse(data);
  // console.log(exits);
});

// turn degree to radius
function toRadians(degree) {
  return degree * Math.PI /180 ;
};

function getDistance(latitude1,longitude1,latitude2,longitude2) {
  //R 是地球半徑，以KM為單位
  var R=6371;
  var deltalatitude = toRadians(latitude2-latitude1);
  var detalongitude = toRadians(longitude2-longitude1);
  latitude1 = toRadians(latitude2);
  var a = Math.sin(deltalatitude/2) * Math.sin(deltalatitude/2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(detalongitude/2) * Math.sin(detalongitude/2);
  var c = 2 * Math.atan(Math.sqrt(a),Math.sqrt(1-a));
  var d = R * c;
  return d;
};

function renderStep(legs, dest_name){
  // parse the JSON object of steps into a string output 
  var output = '';
  var stepCounter = 1;
  // console.log(legs);
  var destination = legs.end_address;
  var dest_coords = legs.end_location;
  var origin = legs.start_address;
  var orig_coords = legs.start_location;
  var coords = [destination, origin];
  var steps = legs.steps;
  
  output = "前往 " + dest_name + "\n";
  
  var $len = steps.length;
  steps.forEach(function(stepObj) {      
      var prefix = '';
      if (stepCounter == 1) {
        // first
        prefix = '首先';
      } else if (stepCounter == $len) {
        // last
        prefix = '最後';
      }
      var instruction = stepObj.html_instructions;
      instruction = instruction.replace(/<[^>]*>/g, ""); // regex to remove html tags 
      var distance = stepObj.distance.text;
      var duration = stepObj.duration.text;
      output += "- " + prefix + instruction + " ("+ distance +"/"+ duration+")\n";
      stepCounter++;
  });
  
  var staticMapUrl = renderStaticMap(legs);
  
  var results = {
    "route": output,
    "address": {
      "origin": origin,
      "destination": dest_name,
    },
    "coords": {
      "origin": orig_coords.lat + "," + orig_coords.lng,
      "destination": dest_coords.lat + "," + dest_coords.lng,
    },
    "map": staticMapUrl
  }
  
  return results;
  // console.log(output);
}

function renderStaticMap(legs){
  var dest_coords = legs.end_location;
  var orig_coords = legs.start_location;
  var params = {
    center: orig_coords.lat + "," + orig_coords.lng,
    zoom: 15,
    size: "400x400",
    scale: 2,
    language: "zh_TW",
    markers: ["size:mid|color:green|label:S|" + orig_coords.lat + "," + orig_coords.lng, "color:red|label:E|" + dest_coords.lat + "," + dest_coords.lng],
    key: api_key
  };
  
  var param = "";
  Object.keys(params).map(function(key, index){
    var value = "";
    if (key == "markers") {
      params[key] = params[key].join('&markers=');
    }
    param += key + "=" + params[key] + "&";
  });
  
  var mapUrl = "https://maps.googleapis.com/maps/api/staticmap?" + param;
  return mapUrl;
}

function getStation(coords, callback){
  googleMapsClient.placesNearby({
    location: coords,
    language: "zh_TW",
    // radius: 2500,
    keyword: "捷運出口",
    rankby: "distance",
    // type: "subway_station"
  }, function(err, response) {
    // console.log("nearby station");
    response.json.results.forEach(function(e, i){
      // console.log(i + ". " + e.name + " (" + JSON.stringify(e.types) + ")");
    });
    callback(response.json.results[0]);
    // return response;
  });
}

function getExit(stationName, coords){
  console.log('station name: ');
  console.log(stationName);
  var match_result = {},
      match_sort = [],
      result = [],
      exitDistance = 0;
  var regex = new RegExp("^" + stationName);
  var exitsResult = exits.filter(function(item){
    return (item.name && regex.test(item.name) && item.exit != 0);
  });
  exitsResult.forEach(function(exit){
    exitDistance = getDistance(coords[0], coords[1], exit.latlng.lat, exit.latlng.lng);
    match_sort.push(exitDistance);
    match_result[exitDistance] = exit;
  });
  match_sort = match_sort.sort(function (a, b){return a - b});

  for (var i = 0; i < match_sort.length; i++) {    
    var end_location = match_result[match_sort[i]].latlng.lat + "," + match_result[match_sort[i]].latlng.lng;
    var start_location = coords[0] + ',' + coords[1];
    
    var legs = { 
      "end_location": {
        "lat": match_result[match_sort[i]].latlng.lat,
        "lng": match_result[match_sort[i]].latlng.lng,
      },
      "start_location": {
        "lat": coords[0],
        "lng": coords[1],
      }
    }
    var staticMapUrl = renderStaticMap(legs);
    // result.push(match_result[match_sort[i]]);
    result.push({
      "exit_name": match_result[match_sort[i]].name + '出口 ' + match_result[match_sort[i]].exit,
      "coords": {
        "origin": start_location,
        "destination": end_location,
      },
      "map": staticMapUrl
    });
  }
  // console.log('distance sort:');
  // console.log(match_sort);

  if (result.length > 0) {
    console.log("Found Exits: ");
    // console.log(exitsResult);
    
    return result;
  } 
  else {
    return false;
  }
}

module.exports = {
  getDirection2MRT: function(currentLocation, destLocation, callback) {
    var nearbyStationCoords = {};
    var dest = getStation(currentLocation, function(nearby) {
      // console.log("nearby station");
      nearbyStationCoords = nearby.geometry.location;
      
      googleMapsClient.directions({
        origin: currentLocation,
        destination: nearbyStationCoords.lat + "," + nearbyStationCoords.lng,
        // destination: "捷運站",
        mode: 'walking',
        language: 'zh_TW'
      }, function(err, response) {
        if (!err) {
          // console.log("response: ")
          // console.log(response.json.routes[0].legs[0]);
          var result = renderStep(response.json.routes[0].legs[0], nearby.name);
          callback(result);
        }
        else {
          console.log(err);
        }
      });
    });

  },
  
  getDirection2Exit: function(stationName, coords, callback){
    var exits = getExit(stationName, coords);
    
    console.log('before gmap.getDirec2Exit callback');
    console.log(exits);
    callback(exits);
    
  }
}