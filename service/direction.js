const fs = require('fs');

var direction = require('google_directions');
var directionKey = "AIzaSyD4u35i9J1RUVWawLlsgprzl6hc-jvVQ2g";

this.output = "";


let exits;
fs.readFile('data/stationExits.json', 'utf8', function (err, data) {
  if (err) throw err;
  exits = JSON.parse(data);
  // console.log(exits);
});

// turn degree to radius
function toRadians(degree) {
  return degree * Math.PI /180 ;
};

function getDistance(latitude1,longitude1,latitude2,longitude2) {
  // console.log('coords')
  // console.log(latitude1,longitude1,latitude2,longitude2);
  
  //R 是地球半徑，以KM為單位
  var R=6371;
  var deltalatitude = toRadians(latitude2-latitude1);
  var detalongitude = toRadians(longitude2-longitude1);
  latitude1 = toRadians(latitude2);
  var a = Math.sin(deltalatitude/2) * Math.sin(deltalatitude/2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(detalongitude/2) * Math.sin(detalongitude/2);
  var c = 2 * Math.atan(Math.sqrt(a),Math.sqrt(1-a));
  var d = R * c;
  return d;
};

module.exports = {
  getDirection: function(origin, dest, mode="walking", callback) {
    var output = "";
    var params = {
        // REQUIRED 
        origin: origin,
        destination: dest,
        key: directionKey,

        // OPTIONAL 
        mode: mode,
        avoid: "",
        language: "zh_TW",
        units: "",
        region: "",
    };
    // console.log(params);
    var directionSteps = direction.getDirectionSteps(params, function (err, steps){
      if (err) {
          console.log("Err: " + err);
          return 1;
      }

      // parse the JSON object of steps into a string output 
      var output = '';
      var stepCounter = 1;
      // console.log(steps);
      steps.forEach(function(stepObj) {
          var instruction = stepObj.html_instructions;
          instruction = instruction.replace(/<[^>]*>/g, ""); // regex to remove html tags 
          var distance = stepObj.distance.text;
          var duration = stepObj.duration.text;
          output += "Step " + stepCounter + ": " + instruction + " ("+ distance +"/"+ duration+")\n";
          stepCounter++;
      });	
      // console.log("output: "+output);
      callback(output);
    });
  },
  
  getDistance: function(latitude1,longitude1,latitude2,longitude2, callback) {
    //R 是地球半徑，以KM為單位
    var R=6371;
    var deltalatitude = toRadians(latitude2-latitude1);
    var detalongitude = toRadians(longitude2-longitude1);
    latitude1 = toRadians(latitude2);
    var a = Math.sin(deltalatitude/2) * Math.sin(deltalatitude/2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(detalongitude/2) * Math.sin(detalongitude/2);
    var c = 2 * Math.atan(Math.sqrt(a),Math.sqrt(1-a));
    var d = R * c;
    callback(d);
  },
  
  getExits: function(stationName, coords, callback){
    console.log('station name: ');
    console.log(stationName);
    var match_result = {},
        match_sort = [],
        result = [],
        exitDistance = 0;
    var regex = new RegExp("^" + stationName);
    var exitsResult = exits.filter(function(item){
      return (item.name && regex.test(item.name) && item.exit != 0);
    });
    exitsResult.forEach(function(exit){
      exitDistance = getDistance(coords[0], coords[1], exit.latlng.lat, exit.latlng.lng);
      match_sort.push(exitDistance);
      match_result[exitDistance] = exit;
    });
    match_sort = match_sort.sort(function (a, b){return a - b});
    
    for (var i = 0; i < match_sort.length; i++) {
      result.push(match_result[match_sort[i]]);
    }
    // console.log('distance sort:');
    // console.log(match_sort);
    
    if (result.length > 0) {
      console.log("Found Exits: ");
      // console.log(exitsResult);

      callback(result);
    } 
    else {
      return false;
    }
  },
  
  getRouteMap: function(){}

}