// This is main file containing code implementing the Express server and functionality for the Express echo bot.
//
'use strict';
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const path = require('path');
const fs = require('fs');
// Add lowdb; lowdb provides in-memory json format db
const low = require('lowdb');
const _ = require('lodash');

// Add customer services
const direction = require('./service/direction.js');
const gmap = require('./service/gmap.js');
                      
var messengerButton = "<html><head><title>Facebook Messenger Bot</title></head><body><h1>Facebook Messenger Bot</h1>This is a bot based on Messenger Platform QuickStart. For more details, see their <a href=\"https://developers.facebook.com/docs/messenger-platform/guides/quick-start\">docs</a>.<footer id=\"gWidget\"></footer><script src=\"https://widget.glitch.me/widget.min.js\"></script></body></html>";
var db = low();
db.defaults({ lastmsg: [], conversation: [], currentLocation: [] })
  .write();

// The rest of the code implements the routes for our Express server.
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

let stations;
fs.readFile('data/stationData.json', 'utf8', function (err, data) {
  if (err) throw err;
  stations = JSON.parse(data);
  // console.log(stations);
});

let distinctStations;
fs.readFile('data/stations.json', 'utf8', function (err, data) {
  if (err) throw err;
  distinctStations = JSON.parse(data);
  // console.log(stations);
});

// Webhook validation
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === process.env.VERIFY_TOKEN) {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);          
  }
});

// Display the web page
app.get('/', function(req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write(messengerButton);
  res.end();
});

// Message processing
app.post('/webhook', function (req, res) {
  console.log(req.body);
  var data = req.body;

  // Make sure this is a page subscription
  if (data.object === 'page') {
    
    // Iterate over each entry - there may be multiple if batched
    data.entry.forEach(function(entry) {
      var pageID = entry.id;
      var timeOfEvent = entry.time;

      // Iterate over each messaging event
      entry.messaging.forEach(function(event) {
        if (event.message) {
          receivedMessage(event);
        } else if (event.postback) {
          receivedPostback(event);   
        } else {
          // console.log("Webhook received unknown event: ", event);
        }
      });
    });

    // Assume all went well.
    //
    // You must send back a 200, within 20 seconds, to let us know
    // you've successfully received the callback. Otherwise, the request
    // will time out and we will keep trying to resend.
    res.sendStatus(200);
  }
});

// Incoming events handling
function receivedMessage(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;
  var modes = ['getDirection', 'searchStation'];
  
  console.log("Received message for user %d and page %d at %d with message:", 
    senderID, recipientID, timeOfMessage);
  console.log("Payload: " + JSON.stringify(message));

  var messageId = message.mid;

  var messageAttachments = message.attachments;
  var messageText = message.text;

  if (message.quick_reply) {
    // console.log(message.quick_reply);
    
    if (modes.indexOf(message.quick_reply.payload) > -1){
      messageText = message.quick_reply.payload;

      // Set conversation mode
      setConversationMode(message.quick_reply.payload);
    }
  }

  var mode = getConversationMode();
  console.log('Mode: ' + mode);

  // console.log("mode: " + mode + "/ " + typeof(mode));
  // console.log(db.value());

// =============================
// 查詢捷運站
// =============================
  
  if (mode == 'searchStation'){
    var foundStation = findStation(messageText, stations);
    if (foundStation) {
      sendStationResult(senderID, foundStation);
      unsetConversationMode();
      return;
    }    
    else if (messageText == 'searchStation'){
      sendTextMessage(senderID, '請輸入要查詢的車站 (' + mode + ')');
      return;
    }
    else {
      sendTextMessage(senderID, '找不到要查詢的車站，請重新輸入 (' + mode + ')');
      return;
    }
  }
  
// =============================
// 找捷運站
// =============================

  if (mode == 'getDirection') {
    senderAction(senderID);

    if (messageAttachments) {
      // console.log(messageAttachments);
      setLocation(messageAttachments);
    }
    
    var destination = '';
    if (message.quick_reply) {
      console.log('Reply destination: ' + message.quick_reply);
      destination = message.quick_reply.payload;
    }
    
    var currentLocation = getLocation();
    
    
    console.log("orgin currentLocation: ");
    console.log(currentLocation);
    if (currentLocation == '' || currentLocation == undefined) {
      sendLocationRequest(senderID);
    } 
    else if (destination == '' || destination == undefined) {
      console.log('in getDirection mode currentLocation: ' + currentLocation);
      getNearStation(getCoordinates(messageAttachments).lat, getCoordinates(messageAttachments).long, 3, function(stations){
        sendStationQuickReply(stations, senderID);
      });
      
    }
    else {
      var destStation = getStation(destination);

      console.log('destStation');
      console.log(destStation);
      
      getExitDirection(senderID, currentLocation, destStation[0].name);
      
      sendTextMessage(senderID, '您可以前往以下捷運出入口');
      // getRouteDirection(senderID, currentLocation, destStation);
      unsetConversationMode();
      unsetLocation();
    }
  }

  if (mode == '' && messageText) {
    // If we receive a text message, check to see if it matches a keyword
    // and send back the template example. Otherwise, just echo the text we received.
    switch (messageText) {
      case 'generic':
      case 'Generic':
      case '基本':
        sendGenericMessage(senderID);
        break;
      case 'location':
      case 'Location':
      case '地點':
        sendLocationRequest(senderID);
        break;
      case 'nearby':
      case 'Nearby':
      case '最近的捷運站':
        setConversationMode('getDirection');
        sendLocationRequest(senderID);
        break;
      case 'route':
      case 'Route':
      case '路徑':
        getRouteDirection(senderID, "25.0422194,121.503361");
        break;
      case 'itinerary':
      case 'Itinerary':
      case '行程':
        sendItinerary(senderID);
        console.log(stations);
        break;
      case 'last':
      case 'Last':
        var msg = getLastMessage();
        console.log(msg); 
        if (msg[0]) {
          sendTextMessage(senderID, msg[0]['message']);
        } else {
          sendTextMessage(senderID, "No previous record!");
        }
        break;
      default:
        // sendTextMessage(senderID, messageText);
        sendQuickReply(senderID);
        saveLastMessage(message.text);
        //console.log(getLastMessage());
    }
  } else if (mode == '' && messageAttachments) {
    sendTextMessage(senderID, "Message with attachment received \n 收到媒體檔案");
    if (messageAttachments[0].type == "location") {
      sendTextMessage(senderID, "Got your geo: " + getCoordinates(messageAttachments).lat + "/" + getCoordinates(messageAttachments).long);
    } else {
      sendTextMessage(senderID, "not location: " + JSON.stringify(messageAttachments["type"]));
    }
  }
}

function receivedPostback(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfPostback = event.timestamp;

  // The 'payload' param is a developer-defined field which is set in a postback 
  // button for Structured Messages. 
  var payload = event.postback.payload;

  console.log("Received postback for user %d and page %d with payload '%s' " + 
    "at %d", senderID, recipientID, payload, timeOfPostback);

  // When a postback is called, we'll send a message back to the sender to 
  // let them know it was successful
  sendTextMessage(senderID, "Postback called");
}

function show_map(position) {
 var latitude = position.coords.latitude;
 var longitude = position.coords.longitude;
 // let's show a map or do something interesting!
 return latitude;
}
//////////////////////////
// Sending helpers
//////////////////////////
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function senderAction(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: "typing_on"
  }
  
  callSendAPI(messageData);
  sleep(2500);
}

function sendTextMessage(recipientId, messageText) {
  // var geo = navigator.geolocation.getCurrentPosition(show_map);
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText
    }
  };

  callSendAPI(messageData);
}

function sendGenericMessage(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            "buttons": [{
                "title":"Webview example",
                "type":"web_url",
                "url":"http://www.example.com",
                "webview_height_ratio":"compact"
            }],
            "image_url": "http://www.example.com/image.png",
            "item_url": "http://www.example.com",
            "subtitle":"It's a TV!",
            "title":"Some TV"
          },
          {
            title: "rift",
            subtitle: "Next-generation virtual reality",
            item_url: "https://m.j91.me/app/#/navigation?30-86",               
            image_url: "http://messengerdemo.parseapp.com/img/rift.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/rift/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for first bubble",
            }],
          }, {
            title: "touch",
            subtitle: "Your Hands, Now in VR",
            item_url: "https://www.oculus.com/en-us/touch/",               
            image_url: "http://messengerdemo.parseapp.com/img/touch.png",
            buttons: [{
              type: "web_url",
              url: "https://www.oculus.com/en-us/touch/",
              title: "Open Web URL"
            }, {
              type: "postback",
              title: "Call Postback",
              payload: "Payload for second bubble",
            }]
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
}

function sendQuickReply(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      text:"需要什麼幫忙呢？",
      quick_replies:[
        {
          content_type:"text",
          title: "前往捷運站",
          payload: "getDirection"
        },
        {
          content_type: "text",
          title: "查詢捷運站",
          payload: "searchStation"
        }
      ]
    }
  };
    
  callSendAPI(messageData);
}

function sendStationQuickReply(stations, recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      text:"這是離你最近的三個車站，請問你要前往哪個車站？",
      quick_replies:[]
    }
  };
  
  stations.forEach(function(station) {
    messageData.message.quick_replies.push({
      content_type: "text",
      title: station.name,
      payload: station.sid
    }); 
  });
    
  callSendAPI(messageData);
}

function sendLocationRequest(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message:{
      text:"請讓我知道你的位置",
      quick_replies:[
        {
          content_type:"location"
        }
      ]
    }
  };
    
  callSendAPI(messageData);
}

function sendStationResult(recipientId, foundStation) {
  // console.log(foundStation);
  var stationElem = [],
      stationArray = [];
  var stationText = '';
  foundStation.forEach(function(station) {
    if (stationArray.indexOf(station.sid) < 0) {
      stationArray.push(station.sid);
      stationElem.push({
        title: station.name + " / " + station.en_name,
        subtitle: station.line_name,
        buttons: [{
          type: "web_url",
          url: "https://mrt.express/platform?" + station.sid,
          title: "月台資訊",
        }]
      });
      // stationText += station.name + ' ' + station.en_name_1 + " " + ((station.en_name_2) ? station.en_name_2 : '') + '\n';
      stationText += station.name + ' ' + station.en_name + '\n';
      
    }
  });
  
  console.log("Station Elem");
  console.log(stationElem);
  
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: stationElem
        }
      }
    }
  }
  callSendAPI(messageData);
  // sendTextMessage(recipientId, "找到的車站: \n" + stationText);  
}

function sendItinerary(recipientId) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "airline_itinerary",
          intro_message: "Here\'s your travel itinerary.",
          locale: "en_US",
          pnr_number: "1234",
          passenger_info: [
            {
              name: "Farbound Smith Jr",
              ticket_number: "0741234567890",
              passenger_id: "p001"
            }
          ],
          flight_info: [
            {
              connection_id: "c001",
              segment_id: "s001",
              flight_number: "KL9123",
              aircraft_type: "Boeing 737",
              departure_airport: {
                airport_code: "SFO",
                city: "San Francisco",
                terminal: "T4",
                gate: "G8"
              },
              arrival_airport: {
                airport_code: "SLC",
                city: "Salt Lake City",
                terminal: "T4",
                gate: "G8"
              },
              flight_schedule: {
                departure_time: "2016-01-02T19:45",
                arrival_time: "2016-01-02T21:20"
              },
              travel_class: "business"
            }
          ],
          passenger_segment_info: [
            {
              segment_id: "s001",
              passenger_id: "p001",
              seat: "73A",
              seat_type: "World Business",
              product_info: [
                {
                  title: "Lounge",
                  value: "Complimentary lounge access"
                },
                {
                  title: "Baggage",
                  value: "1 extra bag 50lbs"
                }
              ]
            }
          ],
          total_price: "14003",
          currency: "USD"
        }
      }
    }

  };
    
  callSendAPI(messageData);
}

function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: process.env.PAGE_ACCESS_TOKEN },
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      // console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }
  });  
}

// Set Express to listen out for HTTP requests
var server = app.listen(process.env.PORT || 3000, function () {
  console.log("Listening on port %s", server.address().port);
});

//////////////////////
// Payload Handler
//////////////////////

// Get Coordinates from Location attachment
function getCoordinates(attachment) {
  var coordinates = {};
  coordinates.lat = attachment[0].payload.coordinates.lat;
  coordinates.long = attachment[0].payload.coordinates.long;
  return coordinates;
}

// Find Station
function findStation(searchText, stations) {
  var regex = new RegExp("^" + searchText + "$");
  var result = stations.filter(function(item){
    return (item.sid && regex.test(item.sid)) || (item.name && item.name.indexOf(searchText) > -1) || (item.en_name && item.en_name.indexOf(searchText) > -1);
  });
  // console.log("Found STATION: " + result);
  
  if (result.length > 0) {
    return result;
  } 
  else {
    return false;
  }
}

function getStation(sid){
  var regex = new RegExp("^" + sid + "$");
  var result = stations.filter(function(item){
    return (item.sid && regex.test(item.sid));
  });
  return result;  
}

// get route to nearest Exit from sent location

function getExitDirection(recipientId, coord, stationName) {
  gmap.getDirection2Exit(stationName, coord, function(results) {
    if (results) {
      console.log('callback getExitDir');
      console.log(results);
      

      var msgElements = [];
      for (var i = 0; i < results.length; i++) {
        msgElements.push(
          {
            "buttons": [{
              "title": "快速導航",
              "type":"web_url",
              "url":"https://www.google.com.tw/maps/dir/" + results[i].coords.origin + "/" +results[i].coords.destination + "/@" + results[i].coords.origin + "/data=!4m2!4m1!3e2",
              "webview_height_ratio":"full",
            }],
            "image_url": results[i].map,
            "item_url": results[i].map,
            // "item_url": "https://www.google.com.tw/maps/dir/" + results[i].coords.origin + "/" +results[i].coords.destination,
            // "subtitle": "出發地：" + results[i].address.origin,
            "title":"前往" + results[i].exit_name,
          }
        );
      }

      var messageData = {
        recipient: {
          id: recipientId
        },
        message: {
          attachment: {
            type: "template",
            payload: {
              template_type: "generic",
              elements: msgElements
            }
          }
        }
      };

      callSendAPI(messageData);
    }
  });
  unsetConversationMode();
}

// get route direction from sent location

function getRouteDirection(recipientId, coords, destCoords) {

  
  gmap.getDirection2MRT(coords, function(results){
    // console.log("callback result");
    // console.log(results);
    sendTextMessage(recipientId, results.route);

    //https://www.google.com.tw/maps/dir/25.0521683,121.5132719/25.048022,121.513129

    var messageData = {
      recipient: {
        id: recipientId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
              "buttons": [{
                  "title": "快速導航",
                  "type":"web_url",
                  "url":"https://www.google.com.tw/maps/dir/" + results.coords.origin + "/" +results.coords.destination + "/@" + results.coords.origin + "/data=!4m2!4m1!3e2",
                  "webview_height_ratio":"compact"
              }],
              "image_url": results.map,
              "item_url": results.map,
              // "item_url": "https://www.google.com.tw/maps/dir/" + results.coords.origin + "/" +results.coords.destination,
              "subtitle": "出發地：" + results.address.origin,
              "title":"前往" + results.address.destination,
            }]
          }
        }
      }
    };  
    console.log("static map url: " + results.map);
    callSendAPI(messageData);
  });
  unsetConversationMode();
}

//////////////////////////////////
// Conversation Handler
//////////////////////////////////

// Store the last message data
function saveLastMessage(message) {
  db.set('lastmsg', [])
    .write();
  
  db.get('lastmsg')
    .push({ message })
    .write();
}

// get the last message data
function getLastMessage() {
  return db.get('lastmsg')
    .value(); 
}

// Set the conversation mode
function setConversationMode(mode) {
  db.set('conversation', [])
    .write();
  db.get('conversation')
    .push({"mode": mode})
    .write();
}

// Get the conversation mode
function getConversationMode() {
  return db.get('conversation')
           .map("mode")
           .value();
}

// Remove the conversation mode
function unsetConversationMode() {
  setConversationMode('');
}

// Set current location
function setLocation(attachment) {
  if (attachment[0].type == "location") {
    // console.log('found location attachment');
    var coords = [];
    coords.push(getCoordinates(attachment).lat);
    coords.push(getCoordinates(attachment).long);
    console.log("coords: ");
    console.log(coords);
    db.set('currentLocation', [])
      .write();
    db.get('currentLocation')
      .push({"coords": coords})
      .write();
  }
}

// Remove saved location
function unsetLocation() {
  db.set('currentLocation',[])
    .write();
}

// Get current location
function getLocation() {
  return db.get('currentLocation')
           .map('coords')
           .first()
           .value();
}

// Get near stations
function getNearStation(lat, lon, count, callback) {
  var match_result = {},
      match_sort = [],
      current_location = [lat,lon],
      result = [];
  
  stations.forEach(function(station) {
    direction.getDistance(current_location[0], current_location[1], station.lat, station.lon, function(distance){
      if (match_sort.indexOf(distance) < 0){
        match_sort.push(distance);
      }
      match_result[distance] = ({sid: station.sid, name: station.name, line_name: station.line_name, en_name: station.en_name, lat: station.lat, lon: station.lon, meter: Math.floor(distance * 1000)});      
    });
  });

  match_sort = match_sort.sort(function (a, b){return a - b});
  
  for (var i = 0; i < count; i++) {
    result.push(match_result[match_sort[i]]);
  }
  callback(result);
  console.log('Near station results: ');
  console.log(result);
}
